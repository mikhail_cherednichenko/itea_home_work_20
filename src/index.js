/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/

/*
1. Зєжнати верстку з джс 
Потрібно зробити цей калькулятор робочим.
2. Знайти всі кнопки та спрбувати їх вивести у консоль
3. Записти перші числа в память коду 
4. вивести числа на екран
5. додати знаки ар. операцій 
6. Знайти другі числа 
7. Вивести другі числа 
8. Вивести результат операції
*/

const calculate = {
    operand1: "",
    sign: "",
    operand2: "",
    rez: "",
    mem: ""
};

const display = document.querySelector(".display input");

const equal = document.getElementById('equal');

const memoryIndicator = document.getElementById('memory');

// https://regexr.com/

document.querySelector(".keys").addEventListener("click", (e) => {
    if (validate(/\d/, e.target.value)) {
        if (calculate.sign == '') {
            calculate.operand1 += e.target.value;
            show(calculate.operand1);
        } else {
            calculate.operand2 += e.target.value;
            if ([...calculate.operand2].length == 1) {
                clearDisplay();
            }
            show(calculate.operand2);
        }
    
    } else if (validate(/^\.$/, e.target.value)) {
        if (calculate.sign == '') {
            if (!validate(/\./, calculate.operand1)) {
                calculate.operand1 += e.target.value;
                show(calculate.operand1);
            };
        } else {
            if (!validate(/\./, calculate.operand2)) {
                calculate.operand2 += e.target.value;
                show(calculate.operand2);
            };
        };

    } else if (validate(/^[+-/*]$/, e.target.value)) {
        if (calculate.operand2 != display.value) {
            calculate.operand2 = '';
        };

        if (calculate.operand1 == '') {
            if (validate(/^-$/, e.target.value)) {
                calculate.operand1 += e.target.value;
                show(calculate.operand1);
            };
        } else {
            calculate.sign = e.target.value;
        };
        
    } else if (validate(/^=$/, e.target.value)) {
        calculate.rez = eval(doubleMinusDelete(calculate.operand1 + calculate.sign + calculate.operand2));
        show(calculate.rez);
        calculate.operand1 = calculate.rez;

    } else if (validate(/^C$/, e.target.value)) {
        clearDisplay();
        calculate.operand1 = "";
        calculate.sign = "";
        calculate.operand2 = "";
        calculate.rez = "";

    } else if (validate(/^m\+$/, e.target.value)) {
        calculate.mem = eval(calculate.mem + '+' + display.value);

    } else if (validate(/^m-$/, e.target.value)) {
        calculate.mem = eval(doubleMinusDelete(calculate.mem + '-' + display.value));

    } else if (validate(/^mrc$/, e.target.value) && calculate.mem !== "") {
        if (calculate.mem == display.value) {
            calculate.mem = '';
        } else {
            if (calculate.sign == '') {
                calculate.operand1 = calculate.mem;
                show(calculate.operand1);
            } else {
                calculate.operand2 = calculate.mem;
                show(calculate.operand2);
            };
        };
        
    };

    if (calculate.mem === '') {
        memoryIndicator.classList.add('invisible');
    } else {
        memoryIndicator.classList.remove('invisible');
    };

    if (calculate.operand1 == "" || calculate.operand2 == "" || calculate.sign == "") {
        equal.disabled = true;
    } else {
        equal.disabled = false;
    };

});

function show (value) {
    display.value = value;
};

function clearDisplay() {
    display.value = "";
};

function doubleMinusDelete(value) {
    return value.replace('--', '+');
};

const validate = (r, v) => r.test(v);


